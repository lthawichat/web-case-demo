import { defineStore } from "pinia";

export const otherFormStore = defineStore("otherForm", {
  state: () => {
    return {
      serviceOpinion: "",
      otherOpinion: "",
    };
  },
});
