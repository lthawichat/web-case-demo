import Axios from "axios";

export const axios = Axios.create();
axios.defaults.timeout = 90000;
